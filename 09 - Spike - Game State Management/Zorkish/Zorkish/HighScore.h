#pragma once
#include "State.h"

class HighScore : public State
{
public:
	HighScore(GameManager *gm);
	void GameLoop();
	void Update();
	void Render();
};