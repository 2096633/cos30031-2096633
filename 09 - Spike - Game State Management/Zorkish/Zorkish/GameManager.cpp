#include "GameManager.h"
#include "MainMenu.h"

GameManager::GameManager()
{
	SetState(new MainMenu(this));
}

void GameManager::SetState(State* state)
{
	currentState = state;
}

void GameManager::StartStateLoop()
{
	currentState->GameLoop();
}

