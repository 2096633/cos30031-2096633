#include "Player.h"
#include <iostream>

Player::Player()
{
	inventory.PutItem(Item("Map", "A slightly torn map"));
}

Inventory* Player::GetInventory()
{
	return &inventory;
}

void Player::SetLocation(Location* l)
{
	currentLocation = l;
}

Location* Player::GetLocation()
{
	return currentLocation;
}
