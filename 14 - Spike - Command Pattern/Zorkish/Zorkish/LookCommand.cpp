#include "LookCommand.h"
#include <iostream>

LookCommand::LookCommand(Player* p)
{
	player = p;
}

void LookCommand::Execute()
{
	if (input.size() > 2)
	{
		if (input[1] == "at")
		{
			std::cout << player->GetLocation()->GetInventory()->GetItem(input[2]).GetDescription() << endl;
		}
		else if (input[1] == "in")
		{
		}
	}
	else
	{
		std::cout << "You can see:\n";
		std::cout << player->GetLocation()->GetDescription() << endl << player->GetLocation()->GetConnections() << player->GetLocation()->GetInventory()->ReturnItems();
	}
}