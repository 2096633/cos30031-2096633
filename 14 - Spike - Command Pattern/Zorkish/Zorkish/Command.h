#pragma once
#include <vector>
#include <string>

#ifndef COMMAND_H
#define COMMAND_H
#endif


class Command
{
public:
	Command();
	virtual void Execute() = 0;
	void SetInput(std::vector<std::string> i);
protected:
	std::vector<std::string> input;
};