#pragma once
#include <vector>
#include <string>
#include "Inventory.h"

#ifndef LOCATION_H
#define LOCATION_H
#endif

using namespace std;

class Location
{
private:
	struct Connection
	{
		string direction;
		Location* target;
	};
	string name;
	string description;
	vector<Connection> connections;
	Inventory inv;
public:
	Location();
	Location(string pName, string desc);
	void AddConnection(string dir, Location* target);
	Location* GetConnection(string dir);
	vector<string> PossibleDirections();
	string GetName();
	string GetDescription();
	Inventory* GetInventory();
	void AddItem(Item i);
	string GetConnections();
};