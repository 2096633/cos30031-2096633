#pragma once
#include <vector>
#include <string>
#include "Item.h"

class Inventory
{
public:
	Item GetItem(std::string s);
	void PutItem(Item i);
	void RemoveItem(std::string s);
	int Size();
	std::string ReturnItems();
	std::string GetInventoryTree();
private:
	std::vector<Item> inv;
};
