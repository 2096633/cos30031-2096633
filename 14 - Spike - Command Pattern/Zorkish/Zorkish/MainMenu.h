#include "State.h"
#pragma once
class MainMenu : public State
{
private:
	bool breakLoop;
public:
	MainMenu(GameManager* manager);
	void GameLoop();
	void Update();
	void Render();
};
