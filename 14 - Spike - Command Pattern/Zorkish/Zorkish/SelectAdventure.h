#pragma once
#include "State.h"

class SelectAdventure : public State
{
private:
	bool breakLoop;
public:
	SelectAdventure(GameManager *manager);
	void GameLoop();
	void Update();
	void Render();
};