#include "SelectAdventure.h"
#include "Gameplay.h"

SelectAdventure::SelectAdventure(GameManager* manager) :State(manager) { breakLoop = false; }

void SelectAdventure::GameLoop()
{
	Render();

	while (!breakLoop)
	{
		Update();
		if(!breakLoop)
			Render();
	}
}

void SelectAdventure::Update()
{
	
	bool inputGood = false;
	char input;
	while (!inputGood)
	{
		std::cin >> input;

		switch (input)
		{
		case '1': //hullbreaker island
			inputGood = true;
			break;
		case '2': //ancient forrest
			inputGood = true;
			break;
		case '3': //sulphur lake
			inputGood = true;
			break;
		default:
			break;
		}
	}
	
	breakLoop = true;
	//choice doesnt matter for testing
	gm->SetState(new Gameplay(gm));
	gm->StartStateLoop();
}

void SelectAdventure::Render()
{
	std::cout << "Choose your adventure:\n\n1. Hullbreaker Island\n2. Ancient Forrest\n3. Sulphur Lake\n\nSelect 1-3:>";
}