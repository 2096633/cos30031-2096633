#pragma once
#include "Command.h"
#include "Player.h"

class InventoryCommand : public Command
{
public:
	InventoryCommand(Player* p);
	void Execute();
private:
	Player* player;
};
