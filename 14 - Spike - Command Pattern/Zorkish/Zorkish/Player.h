#pragma once
#include "Inventory.h"
#include "Location.h"

class Player 
{
private:
	Inventory inventory;
	Location* currentLocation;
public:
	Player();
	Inventory* GetInventory();
	void SetLocation(Location* l);
	Location* GetLocation();
};
