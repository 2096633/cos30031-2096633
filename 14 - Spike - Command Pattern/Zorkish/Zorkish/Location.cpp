#include "Location.h"
#include <iostream>

Location::Location() {}

Location::Location(string pName, string desc)
{
	name = pName;
	description = desc;
}

void Location::AddConnection(string dir, Location * target)
{
	connections.push_back(Connection{dir, target});
}

Location* Location::GetConnection(string dir)
{
	Location* l = new Location();

	for (int i = 0; i < connections.size(); i++)
	{
		if (connections[i].direction == dir)
			l = connections[i].target;
	}
	return l;
}

vector<string> Location::PossibleDirections()
{
	vector<string> result;

	for (int i = 0; i < connections.size(); i++)
		result.push_back(connections[i].direction);

	return result;
}

string Location::GetName()
{
	return name;
}

string Location::GetDescription()
{
	return description;
}

Inventory* Location::GetInventory()
{
	return &inv;
}

void Location::AddItem(Item i)
{
	inv.PutItem(i);
}

string Location::GetConnections()
{
	string output = "There are connecting rooms to the ";

	for (int i = 0; i < connections.size(); i++)
	{
		if (i == (connections.size() - 1))
			output += connections[i].direction + "\n";
		else
			output += connections[i].direction + ", ";
	}
	return output;
}