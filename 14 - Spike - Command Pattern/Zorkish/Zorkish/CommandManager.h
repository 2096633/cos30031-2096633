#pragma once
#include "Command.h"
#include "HelpCommand.h"
#include "LookCommand.h"
#include "InventoryCommand.h"
#include "AliasCommand.h"
#include "DebugTreeCommand.h"
#include "Player.h"
#include "World.h"
#include <string>
#include <vector>
#include <map>

#ifndef COMMANDMANAGER_H
#define COMMANDMANAGER_H
#endif

class AliasCommand;


using namespace std;

class CommandManager
{
public:
	CommandManager();
	CommandManager(Player* p, World* w);
	void PickCommand(string i);
	void SplitInput(string i);
	void ExecuteCommand(Command* c);
	map<string, Command*>* GetCommands();
	vector<string>* GetCommandList();
private:
	vector<string> input;
	Player* player;
	World* world;
	map<string, Command*> commands;
	vector<string> commandInputs;
};
