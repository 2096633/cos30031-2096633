#include "World.h"

void World::ReadWorldInformation()
{
	enum InputState {location, connection, items};
	InputState is = location;
	ifstream in("TestWorld.txt", ios::in);
	string name, desc, dir, target, iName, iDesc;
	bool connections = false;
	while (!in.eof())
	{
		if (is == location)
		{
			getline(in, name, ':');
			getline(in, desc, '\n');
			AddLocation(name, desc);

			if ((char)in.peek() == '\n')
			{
				is = connection;
				in.ignore();
			}
		}
		else if(is == connection)
		{
			getline(in, name, ':');
			getline(in, dir, ':');
			getline(in, target, '\n');
			AddDirection(name, dir, target);

			if ((char)in.peek() == '\n')
			{
				is = items;
				in.ignore();
			}
		}
		else if (is == items)
		{
			getline(in, name, ':');
			getline(in, iName, ':');
			getline(in, iDesc, '\n');
			AddItem(name, iName, iDesc);
		}
	}
	in.close();
}

void World::ReadWorldInformation(string file)
{
	ifstream in(file.c_str(), ios::in);
	enum InputState { location, connection, items };
	InputState is = location;
	string name, desc, dir, target, iName, iDesc;
	bool connections = false;
	while (!in.eof())
	{
		if (is == location)
		{
			getline(in, name, ':');
			getline(in, desc, '\n');
			AddLocation(name, desc);

			if ((char)in.peek() == '\n')
			{
				is = connection;
				in.ignore();
			}
		}
		else if (is == connection)
		{
			getline(in, name, ':');
			getline(in, dir, ':');
			getline(in, target, '\n');
			AddDirection(name, dir, target);

			if ((char)in.peek() == '\n')
			{
				is = items;
				in.ignore();
			}
		}
		else if (is == items)
		{
			getline(in, name, ':');
			getline(in, iName, ':');
			getline(in, iDesc, '\n');
			cout << name << " " << iName << " " << iDesc << endl;
			AddItem(name, iName, iDesc);
		}
	}
	in.close();
}

void World::AddLocation(string lName, string lDesc)
{
	locations.push_back(Location(lName, lDesc));
}

void World::AddDirection(string lName, string dir, string target)
{
	Location* t = new Location();
	Location* r = new Location();
	for(int i = 0; i < locations.size(); i++)
	{

		if (locations[i].GetName() == target)
			t = &locations[i];
		
		if (locations[i].GetName() == lName)
		{
			r = &locations[i];
		}
	}

	if (t->GetName().length() != 0 && r->GetName().length() != 0)
		r->AddConnection(dir, t);
	else
	{
		if (t->GetName().length() == 0)
			cout << "target doesn't exist\n";
		else if (r->GetName().length() == 0)
			cout << "location doesn't exist\n";
	}
}

void World::AddItem(string lName, string iName, string iDesc)
{
	for (int i = 0; i < locations.size(); i++)
	{
		if (locations[i].GetName() == lName)
		{
			locations[i].GetInventory()->PutItem(Item(iName, iDesc));
			return;
		}
	}
}

string World::GetLocationTree()
{
	string output = "->World\n\t->Locations\n";

	for (int i = 0; i < locations.size(); i++)
	{
		output += "\t\t->" + locations[i].GetName() + "\n";
		if(locations[i].GetInventory()->Size() != 0)
			output += locations[i].GetInventory()->GetInventoryTree();
	}

	return output;
}

Location* World::GetStartingLocation()
{
	return &locations[0]; //hard coded test - for real files the start will be indicated with the name of "start"
}
