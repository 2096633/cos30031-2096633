// T02.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

void Task01(int i, int j)
{
	cout << "Accept 2 values, print to screen, return nothing:" << endl <<"Value 1: " << i << endl << "Value 2: " << j << endl;
}

int Task02(int i)
{
	i++;
	return i;
}

void Task04()
{
	cout << "Print odd numbers between 0 and 20 formatted with spaces:" << endl;
	for (int i = 0; i < 20; i++)
	{
		if (i%2 == 1)
			cout << i << " ";
	}
	cout << endl;
}

void Task05()
{
	cout << "Create 1 dimensional array with 5 random numbers:" << endl;
	int random[5];
	for (int i = 0; i < 5; i++)
	{
		random[i] = rand();
		cout << random[i] << " ";
	}
	cout << endl;
}

void Task06()
{
	string s = "this has spaces in it";
	istringstream iss(s);
	cout << "Split the line of text by white spaces:" << endl;
	while (iss)
	{
		string subs;
		iss >> subs;
		cout << subs << endl;
	}

}

class Task07
{
private:
	int p;
public:
	int pub;

	Task07();

	void method();
};

Task07::Task07()
{
	p = 100;
	pub = 4;
}

void Task07::method()
{
	std::cout << "Private variable: " << p << "\n";
	std::cout << "Public variable : " << pub << "\n";
}


int main()
{
	Task01(1, 2);

	cout << "Accept a value, alter value, return value: " << Task02(1) << endl;

	int value;
	int *pointer = &value;
	*pointer = 1;
	cout << "Set and read a pointer: " << *pointer << endl;

	Task04();

	Task05();

	Task06();

	Task07 task;

	task.method();

    return 0;
}

