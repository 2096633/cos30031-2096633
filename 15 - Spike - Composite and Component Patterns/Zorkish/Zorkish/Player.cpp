#include "Player.h"
#include <iostream>

Player::Player()
{
	inventory.PutItem(new Item("Map", "A slightly torn map"));
	health = Health();
}

Inventory* Player::GetInventory()
{
	return &inventory;
}

void Player::SetLocation(Location* l)
{
	currentLocation = l;
}

Location* Player::GetLocation()
{
	return currentLocation;
}

Health * Player::GetHp()
{
	return &health;
}

Damage* Player::GetDamage()
{
	return &damage;
}
