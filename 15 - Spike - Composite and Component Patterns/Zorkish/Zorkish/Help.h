#pragma once
#include "State.h"

class Help : public State
{
public:
	Help(GameManager *gm);
	void GameLoop();
	void Update();
	void Render();
};