#include "GameEntity.h"
#include <algorithm>

GameEntity::GameEntity() { name = ""; }

GameEntity::GameEntity(std::string n, std::string d)
{
	std::transform(n.begin(), n.end(), n.begin(), ::tolower);
	name = n;
	desc = d;
}

std::string GameEntity::GetName()
{
	return name;
}

std::string GameEntity::GetDescription()
{
	return desc;
}