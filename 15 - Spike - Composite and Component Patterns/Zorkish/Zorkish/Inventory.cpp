#include "Inventory.h"
#include <algorithm>
#include <iostream>

using namespace std;

Item* Inventory::GetItem(string s)
{
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	for (int i = 0; i < inv.size(); i++)
	{
		if (inv[i]->GetName() == s)
			return inv[i];
	}
	
	return new Item(s + " not found", s + " not found");
}

void Inventory::PutItem(Item* i)
{
	inv.push_back(i);
}

void Inventory::RemoveItem(string s)
{
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	for (int i = 0; i < inv.size(); i++)
	{
		if (inv[i]->GetName() == s)
		{
			inv.erase(inv.begin() + i);
			return;
		}
	}
}

int Inventory::Size()
{
	return (int)inv.size();
}

std::string Inventory::ReturnItems()
{
	std::string output;

	for (int i = 0; i < inv.size(); i++)
	{
		output += "- " + inv[i]->GetName() + ", " + inv[i]->GetDescription() + "\n";
	}

	if (output == "")
		return "There is nothing in here\n";
	
	return output;
}

std::string Inventory::GetInventoryTree()
{
	std::string output;

	if (inv.size() > 0)
	{
		for (int i = 0; i < inv.size(); i++)
			output += "\t\t\t->" + inv[i]->GetName() + "\n";
		return output;
	}

	return "";
}

