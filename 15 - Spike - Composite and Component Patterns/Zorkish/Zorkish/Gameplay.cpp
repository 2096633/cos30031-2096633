#include "Gameplay.h"
#include "HighScore.h"
#include <string>
#include <algorithm>

Gameplay::Gameplay(GameManager* manager) :State(manager)
{ 
	breakLoop = false;
	cm = CommandManager(&p, &gameWorld);
}

void Gameplay::GameLoop()
{
	SetupGame();

	while (!breakLoop)
	{
		Update();
		if(!breakLoop)
			Render();
	}
}

void Gameplay::SetupGame()
{
	std::cout << "Welcome to Zorkish: Test Land\nThere are now some locations. Please explore!\n";
	gameWorld.ReadWorldInformation();
	p.SetLocation(gameWorld.GetStartingLocation());
	moves = p.GetLocation()->PossibleDirections();
	PossibleMoves();
	cout << ":>";
}

void Gameplay::Update()
{
	string input;
	bool inputGood = false;

	while (!inputGood)
	{
		cin.clear();
		getline(cin, input);
		transform(input.begin(), input.end(), input.begin(), ::tolower);

		if (input == "go")
		{
			cout << "which direction do you want to go?\n:>";
			cin >> input;
			transform(input.begin(), input.end(), input.begin(), ::tolower);
			Location* here = p.GetLocation();

			if (input == "n")
				input = "north";
			else if (input == "e")
				input = "east";
			else if (input == "s")
				input = "south";
			else if (input == "w")
				input = "west";
			else if (input == "ne")
				input = "northeast";
			else if (input == "se")
				input == "southeast";
			else if (input == "sw")
				input = "southwest";
			else if (input == "nw")
				input = "northwest";
			else if (input == "u")
				input = "up";
			else if (input == "d")
				input = "down";

			if (here->GetDoorByDir(input) != nullptr)
			{
				if (here->GetDoorByDir(input)->IsLocked())
				{
					std::cout << "The door is locked, you'll have to find another way\n";
					inputGood = !inputGood;
				}
				else
				{
					for (int i = 0; i < moves.size(); i++)
					{
						if (moves[i] == input)
						{
							p.SetLocation(p.GetLocation()->GetConnection(input));
							cout << "You move " << input << endl;
							inputGood = !inputGood;
						}
					}
					if (here == p.GetLocation())
						std::cout << "There isn't a path there.\n:>";
				}
			}
			else
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == input)
					{
						p.SetLocation(p.GetLocation()->GetConnection(input));
						inputGood = !inputGood;
					}
				}
				if (here == p.GetLocation())
					std::cout << "There isn't a path there.\n:>";
			}
		}
		else if (input == "quit")
		{
			inputGood = !inputGood;
			breakLoop = true;
		}
		else if (input == "hiscore")
		{
			inputGood = !inputGood;
			breakLoop = true;
			gm->SetState(new HighScore(gm));
			gm->StartStateLoop();
		}
		else if (input != "")
		{
			cm.PickCommand(input);
			inputGood = !inputGood;
		}

	}

	moves = p.GetLocation()->PossibleDirections();
}

void Gameplay::Render()
{
	PossibleMoves();
	if (p.GetLocation()->GetEnemies()->size() > 0)
	{
		vector<Enemy> e = *p.GetLocation()->GetEnemies();
		for (int i = 0; i < e.size(); i++)
		{
			cout << "A " << e[i].GetName() << " has appeared! " << e[i].GetDescription() << endl;
		}
	}
	cout << ":>";
}

void Gameplay::PossibleMoves()
{
	std::cout << "You can move: ";

	for (int i = 0; i < moves.size(); i++)
	{
		if (i == (moves.size() - 1))
			std::cout << moves[i] << endl;
		else
			std::cout << moves[i] << ", ";
	}
}