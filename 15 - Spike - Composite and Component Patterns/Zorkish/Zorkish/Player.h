#pragma once
#include "Inventory.h"
#include "Location.h"
#include "Health.h"

class Player 
{
private:
	Inventory inventory;
	Location* currentLocation;
	Health health;
	Damage damage;
public:
	Player();
	Inventory* GetInventory();
	void SetLocation(Location* l);
	Location* GetLocation();
	Health* GetHp();
	Damage* GetDamage();
};
