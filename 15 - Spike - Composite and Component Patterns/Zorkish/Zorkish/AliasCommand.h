#pragma once
#include "Command.h"
#include "CommandManager.h"
#include <vector>
#include <string>
#include <map>

#ifndef ALIASCOMMAND_H
#define ALIASCOMMAND_H
#endif

using namespace std;

class CommandManager;

class AliasCommand : public Command
{
public:
	AliasCommand(CommandManager& c);
	void Execute();
private:
	CommandManager* cm;
};
