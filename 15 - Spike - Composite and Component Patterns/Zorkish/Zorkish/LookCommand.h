#pragma once
#include "Command.h"
#include "Player.h"

class LookCommand : public Command
{
public:
	LookCommand(Player* p);
	void Execute();
private:
	Player* player;
};
