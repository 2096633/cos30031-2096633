#pragma once
#include "GameEntity.h"
#include "Lock.h"
#include "Item.h"

class Door : public GameEntity
{
public:
	Door(std::string n, std::string d, std::string dr, Item i);
	bool IsLocked();
	void Unlock(Item k);
	std::string GetDirection();
private:
	std::string dir;
	Lock lock;
};