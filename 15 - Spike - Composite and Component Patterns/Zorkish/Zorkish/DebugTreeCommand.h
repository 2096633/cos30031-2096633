#pragma once
#include "Command.h"
#include "World.h"
#include "Player.h"

class DebugTreeCommand : public Command
{
public:
	DebugTreeCommand(Player* p, World* w);
	void Execute();
private:
	Player* player;
	World* gameWorld;
};
