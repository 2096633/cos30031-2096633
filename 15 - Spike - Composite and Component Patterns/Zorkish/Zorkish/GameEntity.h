#pragma once
#include <string>
#include <vector>

class GameEntity
{
public:
	GameEntity();
	GameEntity(std::string n, std::string d);
	std::string GetName();
	std::string GetDescription();
protected:
	std::string name;
	std::string desc;
};
