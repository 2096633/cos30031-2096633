#pragma once
#include "State.h"

class About : public State
{
public:
	About(GameManager *gm);
	void GameLoop();
	void Update();
	void Render();
};