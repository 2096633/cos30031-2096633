#include "LookCommand.h"
#include "Container.h"
#include <iostream>

LookCommand::LookCommand(Player* p)
{
	player = p;
}

void LookCommand::Execute()
{
	if (input.size() == 4)
	{
		if (input[1] == "at")
		{
			Item* iL = player->GetLocation()->GetInventory()->GetItem(input[2]);
			Item* iP = player->GetInventory()->GetItem(input[2]);
			if (iL->GetName() == input[2])
				cout << iL->GetDescription() << endl;
			else if(iP->GetName() == input[2])
				cout << player->GetInventory()->GetItem(input[2])->GetDescription() << endl;
			else
			{
				if (player->GetLocation()->GetDoor(input[2]) != nullptr)
					cout << player->GetLocation()->GetDoor(input[2])->GetDescription() << endl;
				else if (player->GetLocation()->GetEnemy(input[2]) != nullptr)
					cout << player->GetLocation()->GetEnemy(input[2])->GetDescription() << endl;
			}
		}
		else if (input[1] == "in")
		{
			if (input[2] == "inventory")
				cout << player->GetInventory()->ReturnItems();
			else
			{
				Item* i = player->GetLocation()->GetInventory()->GetItem(input[2]);
				if (!i->IsContainer())
				{
					if (i->GetName() != input[2])
						cout << i->GetName() << endl; //returns name + "not found"
					else
						cout << "You can't look inside a " << input[2] << endl;
				}
				else
				{
					Container* c = static_cast<Container*> (player->GetLocation()->GetInventory()->GetItem(input[2]));
					cout << "Inside the " << c->GetName() << " theres:" << endl;
					cout << c->GetContents()->ReturnItems();
				}
			}
		}
	}
	else if (input.size() == 2)
	{
		cout << "You can see:\n";
		cout << player->GetLocation()->GetDescription() << endl << player->GetLocation()->GetConnections() << player->GetLocation()->GetInventory()->ReturnItems();
		for (int i = 0; i < player->GetLocation()->GetDoors()->size(); i++)
			cout << "There is a " << player->GetLocation()->GetDoors()->operator[](i).GetName() << "to the " << player->GetLocation()->GetDoors()->operator[](i).GetDirection() << endl;
		for (int i = 0; i < player->GetLocation()->GetEnemies()->size(); i++)
			cout << player->GetLocation()->GetEnemies()->operator[](i).GetName() << " " << player->GetLocation()->GetEnemies()->operator[](i).GetDescription() <<" it is " <<  boolalpha << player->GetLocation()->GetEnemies()->operator[](i).IsDed() << endl;
	}
	else
		cout << "invalid command" << endl;
}