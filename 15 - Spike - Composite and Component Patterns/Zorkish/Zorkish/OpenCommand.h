#pragma once
#include "Command.h"
#include "Player.h"

class OpenCommand : public Command
{
public:
	OpenCommand(Player*);
	void Execute();
private:
	Player* player;
};
