#pragma once
#include "GameEntity.h"
#include "Health.h"
#include "Damage.h"
#include "Inventory.h"

class Enemy : public GameEntity
{
public:
	Enemy();
	Enemy(std::string n, std::string d, int h, int dmg);
	Inventory* GetInventory();
	Health* GetHp();
	Damage* GetDPS();
	bool IsDed();
private:
	Damage damage;
	Health health;
	Inventory inv;
	bool isDed;
};
