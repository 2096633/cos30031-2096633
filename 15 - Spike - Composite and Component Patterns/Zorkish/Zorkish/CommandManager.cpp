#include "CommandManager.h"
#include <iostream>
#include <sstream>
#include <algorithm>

CommandManager::CommandManager() {}

CommandManager::CommandManager(Player* p, World* w)
{
	player = p;
	world = w;

	commandInputs.push_back("look");
	commandInputs.push_back("inventory");
	commandInputs.push_back("help");
	commandInputs.push_back("alias");
	commandInputs.push_back("debug");
	commandInputs.push_back("take");
	commandInputs.push_back("put");
	commandInputs.push_back("open");

	commands[commandInputs[0]] = new LookCommand(player);
	commands[commandInputs[1]] = new InventoryCommand(player);
	commands[commandInputs[2]] = new HelpCommand();
	commands[commandInputs[4]] = new DebugTreeCommand(player, world);
	commands[commandInputs[5]] = new TakeCommand(player);
	commands[commandInputs[6]] = new PutCommand(player);
	commands[commandInputs[7]] = new OpenCommand(player);
	commands[commandInputs[3]] = new AliasCommand(*this);
}

void CommandManager::PickCommand(string i)
{
	SplitInput(i);

	for (int index = 0; index < commands.size(); index++)
	{
		if (commands[commandInputs[index]] == commands["alias"])
			commands[commandInputs[index]] = new AliasCommand(*this);
		commands[commandInputs[index]]->SetInput(input);
	}

	map<string, Command*>::iterator it;
	it = commands.find(input[0]);

	if (it != commands.end())
		ExecuteCommand(commands[input[0]]);
}

void CommandManager::SplitInput(string i)
{
	input.clear();

	stringstream iss(i);
	while (iss)
	{
		string o;
		iss >> o;
		transform(o.begin(), o.end(), o.begin(), ::tolower);
		input.push_back(o);
	}
}

void CommandManager::ExecuteCommand(Command* c)
{
	c->Execute();
}

map<string, Command*>* CommandManager::GetCommands()
{
	return &commands;
}

vector<string>* CommandManager::GetCommandList()
{
	return &commandInputs;
}
