#include "InventoryCommand.h"
#include <iostream>

InventoryCommand::InventoryCommand(Player* p)
{
	player = p;
}

void InventoryCommand::Execute()
{
	std::cout << player->GetInventory()->ReturnItems();
}