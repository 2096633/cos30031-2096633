#include "Door.h"

Door::Door(std::string n, std::string d, std::string dr, Item i) : GameEntity(n, d)
{
	dir = dr;
	lock = Lock(i);
}

bool Door::IsLocked()
{
	return lock.IsLocked();
}

void Door::Unlock(Item k)
{
	lock.Unlock(k);
}

std::string Door::GetDirection()
{
	return dir;
}
