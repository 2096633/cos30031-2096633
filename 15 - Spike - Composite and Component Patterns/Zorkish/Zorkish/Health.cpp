#include "Health.h"

Health::Health()
{
	maxHealth = 10;
	currentHealth = maxHealth;
}

Health::Health(int max)
{
	maxHealth = max;
	currentHealth = max;
}

int Health::GetCurrentHealth()
{
	return currentHealth;
}

void Health::AdjustHealth(int amt)
{
	currentHealth += amt;
	if (currentHealth >= maxHealth)
		currentHealth = maxHealth;
}
