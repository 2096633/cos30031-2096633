#include "Enemy.h"

Enemy::Enemy(std::string n, std::string d, int h, int dmg) : GameEntity(n, d)
{
	health = Health(h);
	damage = Damage(dmg);
	if (health.GetCurrentHealth() < 0)
		isDed = false;
	else
		isDed = true;
}

Inventory * Enemy::GetInventory()
{
	if (isDed)
		return &inv;
	else
		return nullptr;
}

Health * Enemy::GetHp()
{
	return &health;
}

Damage * Enemy::GetDPS()
{
	return &damage;
}

bool Enemy::IsDed()
{
	return isDed;
}
