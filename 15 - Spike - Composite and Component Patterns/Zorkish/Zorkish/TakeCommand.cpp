#include "TakeCommand.h"
#include "Container.h"
#include <iostream>

TakeCommand::TakeCommand(Player* p)
{
	player = p;
}

void TakeCommand::Execute()
{
	if (input.size() == 5 && input[2] == "from")
	{
		Item* i = player->GetLocation()->GetInventory()->GetItem(input[3]);
		if (i->IsContainer())
		{
			Container* c = static_cast<Container*>(player->GetLocation()->GetInventory()->GetItem(input[3]));
			if (c->GetContents()->GetItem(input[1])->GetName() == input[1])
			{
				player->GetInventory()->PutItem(c->GetContents()->GetItem(input[1]));
				c->GetContents()->RemoveItem(input[1]);
			}
			else
				cout << input[1] << " could not be found in " << input[3] << endl;
		}
		else if (player->GetInventory()->GetItem(input[3])->IsContainer())
		{
			Container* c = static_cast<Container*>(player->GetInventory()->GetItem(input[1]));
			if (c->GetContents()->GetItem(input[1])->GetName() == input[1])
			{
				player->GetInventory()->PutItem(c->GetContents()->GetItem(input[1]));
				c->GetContents()->RemoveItem(input[1]);
			}
		}
	}
	else if (input.size() ==3 )
	{
		Item* i = player->GetLocation()->GetInventory()->GetItem(input[1]);
		if (i->GetName() == input[1])
		{
			player->GetInventory()->PutItem(i);
			player->GetLocation()->GetInventory()->RemoveItem(input[1]);
			std::cout << "You have taken the " << i->GetName() << std::endl;
		}
	}
}