#include "World.h"
#include "Container.h"
#include "Door.h"
#include "Enemy.h"

void World::ReadWorldInformation()
{
	enum InputState {location, connection, items, containers, doors, enemies};
	InputState is = location;
	ifstream in("TestWorld.txt", ios::in);
	string name, desc, dir, target, iName, iDesc, boolean, cName;
	int hp, dmg;
	bool container = false;
	while (!in.eof())
	{
		switch (is)
		{
		case location:
			getline(in, name, ':');
			getline(in, desc, '\n');
			AddLocation(name, desc);

			if ((char)in.peek() == '\n')
			{
				is = connection;
				in.ignore();
			}
			break;
		case connection:
			getline(in, name, ':');
			getline(in, dir, ':');
			getline(in, target, '\n');
			AddDirection(name, dir, target);

			if ((char)in.peek() == '\n')
			{
				is = items;
				in.ignore();
			}
			break;
		case items:
			getline(in, name, ':');
			getline(in, iName, ':');
			getline(in, iDesc, ':');
			getline(in, boolean, '\n');
			container = (boolean == "true") ? true : false;
			AddItem(name, iName, iDesc, container);

			if ((char)in.peek() == '\n')
			{
				is = containers;
				in.ignore();
			}
			break;
		case containers:
			getline(in, name, ':');
			getline(in, cName, ':');
			getline(in, iName, ':');
			getline(in, iDesc, '\n');
			AddToContainer(name, cName, iName, iDesc);

			if ((char)in.peek() == '\n')
			{
				is = doors;
				in.ignore();
			}
			break;
		case doors:
			getline(in, name, ':');
			getline(in, cName, ':');
			getline(in, desc, ':');
			getline(in, dir, ':');
			getline(in, iName, ':');
			getline(in, iDesc, '\n');
			AddDoor(name, cName, desc, dir, iName, iDesc);

			if ((char)in.peek() == '\n')
			{
				is = enemies;
				in.ignore();
			}
			break;
		case enemies:
			getline(in, name, ':');
			getline(in, iName, ':');
			getline(in, iDesc, ':');
			getline(in, cName, ':');
			getline(in, dir, ':');
			hp = stoi(cName);
			dmg = stoi(dir);
			AddEnemy(name, iName, iDesc, hp, dmg);
		default:
			break;
		}
	}
	in.close();
}

void World::ReadWorldInformation(string file)
{
	ifstream in(file.c_str(), ios::in);
	enum InputState { location, connection, items, containers, entities };
	InputState is = location;
	string name, desc, dir, target, iName, iDesc, boolean;
	bool container;
	while (!in.eof())
	{
		if (is == location)
		{
			getline(in, name, ':');
			getline(in, desc, '\n');
			AddLocation(name, desc);

			if ((char)in.peek() == '\n')
			{
				is = connection;
				in.ignore();
			}
		}
		else if (is == connection)
		{
			getline(in, name, ':');
			getline(in, dir, ':');
			getline(in, target, '\n');
			AddDirection(name, dir, target);

			if ((char)in.peek() == '\n')
			{
				is = items;
				in.ignore();
			}
		}
		else if (is == items)
		{
			getline(in, name, ':');
			getline(in, iName, ':');
			getline(in, iDesc, ':');
			getline(in, boolean, '\n');
			container = (boolean == "true") ? true : false;
			AddItem(name, iName, iDesc, container);
		}
	}
	in.close();
}

void World::AddLocation(string lName, string lDesc)
{
	locations.push_back(Location(lName, lDesc));
}

void World::AddDirection(string lName, string dir, string target)
{
	Location* t = new Location();
	Location* r = new Location();
	for(int i = 0; i < locations.size(); i++)
	{

		if (locations[i].GetName() == target)
			t = &locations[i];
		
		if (locations[i].GetName() == lName)
		{
			r = &locations[i];
		}
	}

	if (t->GetName().length() != 0 && r->GetName().length() != 0)
		r->AddConnection(dir, t);
	else
	{
		if (t->GetName().length() == 0)
			cout << "target doesn't exist\n";
		else if (r->GetName().length() == 0)
			cout << "location doesn't exist\n";
	}
}

void World::AddItem(string lName, string iName, string iDesc, bool c)
{
	for (int i = 0; i < locations.size(); i++)
	{
		if (locations[i].GetName() == lName)
		{
			if(c)
				locations[i].GetInventory()->PutItem(new Container(iName, iDesc));
			else
				locations[i].GetInventory()->PutItem(new Item(iName, iDesc));
			return;
		}
	}
}

void World::AddToContainer(string loc, string cName, string iName, string iDesc)
{
	for (int i = 0; i < locations.size(); i++)
	{
		if (locations[i].GetName() == loc)
		{
			Container* c = static_cast<Container*>(locations[i].GetInventory()->GetItem(cName));
			c->GetContents()->PutItem(new Item(iName, iDesc));
		}
	}
}

void World::AddEnemy(string loc, string eName, string eDesc, int hp, int dmg)
{
	for (int i = 0; i < locations.size(); i++)
	{
		if (locations[i].GetName() == loc)
		{
			locations[i].AddEnemy(Enemy(eName, eDesc, hp, dmg));
			return;
		}
	}
}

void World::AddDoor(string loc, string dName, string dDesc, string dir, string kName, string kDesc)
{
	for (int i = 0; i < locations.size(); i++)
	{
		if (locations[i].GetName() == loc)
		{
			locations[i].AddDoor(Door(dName, dDesc, dir, Item(kName, kDesc)));
			return;
		}
	}
}

void World::AddComponent(string loc, string iName, int dmg)
{
	for (int i = 0; i < locations.size(); i++)
	{
		if (locations[i].GetName() == loc)
		{
			locations[i].GetInventory()->GetItem(iName)->SetDamage(dmg);
			return;
		}
	}
}

string World::GetLocationTree()
{
	string output = "->World\n\t->Locations\n";

	for (int i = 0; i < locations.size(); i++)
	{
		output += "\t\t->" + locations[i].GetName() + "\n";
		if(locations[i].GetInventory()->Size() != 0)
			output += locations[i].GetInventory()->GetInventoryTree();
	}

	return output;
}

Location* World::GetStartingLocation()
{
	return &locations[0]; //hard coded test - for real files the start will be indicated with the name of "start"
}
