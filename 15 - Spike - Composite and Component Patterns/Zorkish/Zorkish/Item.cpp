#include "Item.h"
#include <algorithm>
#include <iostream>

Item::Item() : GameEntity("item not found","item not found") {}

Item::Item(std::string pName, std::string pDesc) : GameEntity(pName, pDesc) 
{
	isC = false; 
	damage = Damage();
}

Item::Item(const Item &i2)
{
	name = i2.name;
	desc = i2.desc;
	damage = Damage();
	if (i2.isC)
		isC = true;
	else
		isC = false;
}

bool Item::IsContainer()
{
	return isC;
}

Damage* Item::GetDamage()
{
	return &damage;
}

void Item::SetDamage(int d)
{
	damage = Damage(d);
}

bool Item::operator==(const Item & rhs) const
{
	if (name == rhs.name && desc == rhs.desc)
		return true;
	return false;
}
