#pragma once
#include "State.h"

#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
#endif

class State;

class GameManager
{
public:
	GameManager();
	void StartStateLoop();
	void SetState(State* state);
private:
	State* currentState;
};