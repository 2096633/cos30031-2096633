#include "Damage.h"

Damage::Damage()
{
	damage = 0;
}

Damage::Damage(int dmg)
{
	damage = dmg;
}

int Damage::GetDamage()
{
	return damage;
}
