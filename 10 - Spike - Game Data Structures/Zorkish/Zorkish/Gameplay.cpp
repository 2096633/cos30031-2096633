#include "Gameplay.h"
#include <string>
#include <algorithm>
#include "HighScore.h"

Gameplay::Gameplay(GameManager* manager) :State(manager) { breakLoop = false; }

void Gameplay::GameLoop()
{
	Render();

	while (!breakLoop)
	{
		Update();
		if(!breakLoop)
			Render();
	}
}

void Gameplay::Update()
{
	std::string input;
	bool inputGood = false;

	while (!inputGood)
	{
		std::cin >> input;
		std::transform(input.begin(), input.end(), input.begin(), ::tolower);
		if (input == "quit")
		{
			inputGood = true;
			breakLoop = true;
		}
		if (input == "hiscore")
		{
			inputGood = true;
			breakLoop = true;
			gm->SetState(new HighScore(gm));
			gm->StartStateLoop();
		}
	}
}

void Gameplay::Render()
{
	std::cout << "Welcome to Zorkish: Test Land\nThere is nothing of value here. Time to leave.\n:>";
}