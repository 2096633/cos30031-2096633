#include "Inventory.h"
#include <algorithm>
#include <iostream>

using namespace std;

Inventory::Inventory() 
{
	PutItem(Item("Map", "A torn map of the area"));
}

Item Inventory::GetItem(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	for (int i = 0; i < inv.size(); i++)
	{
		if (inv[i].GetName() == s)
			return inv[i];
	}
	
	return Item("Item " + s + " not found", "Item" + s + " not found");
}

void Inventory::PutItem(Item i)
{
	inv.push_back(i);
}

void Inventory::RemoveItem(std::string s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	for (int i = 0; i < inv.size(); i++)
	{
		if (inv[i].GetName() == s)
		{
			inv.erase(inv.begin() + i);
			return;
		}
	}
}

int Inventory::Size()
{
	return inv.size();
}

