#include "Help.h"

Help::Help(GameManager* mananger) :State(mananger) {}

void Help::GameLoop()
{
	Render();
	Update();
}

void Help::Update()
{
	std::cin.get();
	std::cin.get();
}

void Help::Render()
{
	std::cout << "The following commands are supported:\n-quit\n-hiscore (for testing)\n\nPress Enter to return to the Main Menu...";
}