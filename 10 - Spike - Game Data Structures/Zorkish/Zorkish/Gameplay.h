#pragma once
#include "State.h"
#include <string>

class Gameplay : public State
{
private:
	bool breakLoop;
public:
	Gameplay(GameManager *gm);
	void GameLoop();
	void Update();
	void Render();
};