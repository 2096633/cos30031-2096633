// GridWorld.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

//Hard coded map
char map[8][8] = {
{'#','#','#','#','#','#','#','#'},
{'#','G',' ','D','#','D',' ','#'},
{'#',' ',' ',' ','#',' ',' ','#'},
{'#','#','#',' ','#',' ','D','#'},
{'#',' ',' ',' ','#',' ',' ','#'},
{'#',' ','#','#','#','#',' ','#'},
{'#',' ',' ',' ',' ',' ',' ','#'},
{'#','#','S','#','#','#','#','#'} };
//Game over bool for while game loop
bool gameOver = false;

//Player position variables (defaults to S on map)
int x = 7, y = 2;

//Check valid moves for player
bool CheckMove(int x, int y)
{
	if (x > 7 || y > 7) //out of range exception
		return false;

	if (map[x][y] == '#') //your not a ghost, no walking through walls
		return false;
	else //Either white space or gold or maybe ded?
		return true;

}

//Check valid moves for player
void ValidMoves()
{
	cout << "You can move: ";
	if (map[x - 1][y] != '#')
		cout << "N ";
	if (map[x][y - 1] != '#')
		cout << "E ";
	if (map[x + 1][y] != '#' && (x+1) < 8)
		cout << "S ";
	if (map[x][y + 1] != '#')
		cout << "W ";
	cout << ">";
}

//Update the map
void Update()
{
	ValidMoves();
	bool goodMove = false;
	while (!goodMove) //while player doesn't enter a valid move option keep looping until input is deemed valid
	{
		char input;
		cin >> input;
		switch (tolower(input)) //convert input to lower case
		{
		case('n'):
			if(CheckMove(x-1, y))
				x--;
			break;
		case('e'):
			if(CheckMove(x, y+1))
				y++;
			break;
		case('s'):
			if(CheckMove(x+1, y))
				x++;
			break;
		case('w'):
			if(CheckMove(x, y-1))
				y--;
			break;
		case('q'):
			gameOver = !gameOver;
		default:
			ValidMoves();
		}

		//check if a valid move has been entered
		goodMove = CheckMove(x, y);
		if (goodMove)
		{
			if (map[x][y] == 'D' || map[x][y] == 'G') //you either good or ded
				gameOver = !gameOver;
		}
	}
}

//Render out map and player position
void Render()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (i == x && j == y)
				cout << 'P';
			else
				cout << map[i][j];
		}
		cout << endl;
	}
}

int main()
{
	//intro print
	cout << "Welcome to GridWorld: Quantised Excitement. Fate is waiting for you!\nValid commands: N, S, E and W for direction. Q to quit the game.\n";
	Render();
	//game loop
	while (!gameOver)
	{
		Update();
		Render();
	}
	if (map[x][y] == 'D')
		cout << "You have fallen into a pit.\nYou are ded!\nBetter luck next time!\n";
	else if (map[x][y] == 'G')
		cout << "You found the lost chest of gold.\nYou win!\nThanks for playing!\n";
	else
		cout << "You have quit.\nCome back when you think you're ready!\n";
    return 0;
}

