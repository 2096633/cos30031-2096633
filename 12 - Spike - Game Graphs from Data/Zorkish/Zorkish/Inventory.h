#pragma once
#include <vector>
#include <string>
#include "Item.h"

class Inventory
{
public:
	Inventory();
	Item GetItem(std::string s);
	void PutItem(Item i);
	void RemoveItem(std::string s);
	int Size();
private:
	std::vector<Item> inv;
};
