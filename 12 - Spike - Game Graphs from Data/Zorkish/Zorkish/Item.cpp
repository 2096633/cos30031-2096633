#include "Item.h"
#include <algorithm>

Item::Item(std::string pName, std::string pDesc)
{
	std::string temp = pName;
	std::transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
	name = temp;
	desc = pDesc;
}

Item::Item(const Item &i2)
{
	name = i2.name;
	desc = i2.desc;
}

std::string Item::GetName()
{
	return name;
}

std::string Item::GetDescription()
{
	return desc;
}