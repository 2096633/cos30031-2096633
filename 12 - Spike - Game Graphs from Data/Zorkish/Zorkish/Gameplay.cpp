#include "Gameplay.h"
#include <string>
#include <algorithm>
#include "HighScore.h"

Gameplay::Gameplay(GameManager* manager) :State(manager) { breakLoop = false; }

void Gameplay::GameLoop()
{
	SetupGame();

	while (!breakLoop)
	{
		Update();
		if(!breakLoop)
			Render();
	}
}

void Gameplay::SetupGame()
{
	cout << "Welcome to Zorkish: Test Land\nThere are now some locations. Please explore!\n";
	gameWorld.ReadWorldInformation();
	p.SetLocation(gameWorld.GetStartingLocation());
	moves = p.GetLocation()->PossibleDirections();
	PossibleMoves();
}

void Gameplay::Update()
{
	string input;
	bool inputGood = false;

	while (!inputGood)
	{
		cin >> input;
		transform(input.begin(), input.end(), input.begin(), ::tolower);

		if (input == "go")
		{
			cin >> input;
			transform(input.begin(), input.end(), input.begin(), ::tolower);
			Location* here = p.GetLocation();
			if (input == "north" || input == "n")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("north"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("north"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "east" || input == "e")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("east"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("east"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "south" || input == "s")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("south"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("south"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "west" || input == "w")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("west"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("west"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "northeast" || input == "ne" || input =="north east")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("northeast"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("northeast"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "southeast" || input == "se" || input == "south east")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("southeast"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("southeast"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "southwest" || input == "sw" || input == "south west")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("southwest"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("southwest"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "northwest" || input == "nw" || input == "north west")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("northwest"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("northwest"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "up" || input == "u")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("up"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("up"));
					}
				}
				inputGood = !inputGood;
			}
			else if (input == "down" || input == "d")
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == ("down"))
					{
						p.SetLocation(p.GetLocation()->GetConnection("down"));
					}
				}
				inputGood = !inputGood;
			}
			else
				cout << "Invalid direction\n";

			if (here = p.GetLocation())
				cout << "There isn't a path there.\n";
		}
		else if (input == "quit")
		{
			inputGood = !inputGood;
			breakLoop = true;
		}
		else if (input == "hiscore")
		{
			inputGood = !inputGood;
			breakLoop = true;
			gm->SetState(new HighScore(gm));
			gm->StartStateLoop();
		}
	}

	moves = p.GetLocation()->PossibleDirections();
}

void Gameplay::Render()
{
	PossibleMoves();
	cout << endl << p.GetLocation()->GetName() << endl; //to show that the location is changing
}

void Gameplay::PossibleMoves()
{
	cout << "You can move: ";

	for (int i = 0; i < moves.size(); i++)
	{
		if (i == (moves.size() - 1))
			cout << moves[i] << endl << ":>";
		else
			cout << moves[i] << ", ";
	}
}