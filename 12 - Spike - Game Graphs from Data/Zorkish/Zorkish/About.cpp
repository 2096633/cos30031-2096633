#include "About.h"

About::About(GameManager* manager) :State(manager) {}

void About::GameLoop()
{
	Render();
	Update();
}

void About::Update()
{
	std::cin.get();
	std::cin.get();	
}

void About::Render()
{
	std::cout << "Written by: Daniel Schurmann - 2096633\nPress Enter to return to the Main Menu...\n";
}