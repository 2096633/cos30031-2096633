#pragma once
#include <string>

class Item
{
public:
	Item(std::string pName, std::string pDesc);
	Item(const Item &i2);
	std::string GetName();
	std::string GetDescription();
private:
	std::string name;
	std::string desc;
};