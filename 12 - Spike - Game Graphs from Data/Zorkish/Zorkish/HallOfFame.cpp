#include "HallOfFame.h"

HallOfFame::HallOfFame(GameManager* manager) :State(manager) {}

void HallOfFame::GameLoop()
{
	Render();
	Update();
}

void HallOfFame::Update()
{
	std::cin.get();
	std::cin.get();
}

void HallOfFame::Render()
{
	std::cout << "Top 10 Zorkish Adventure Champions\n\n1. Jimmy, Sulphur Lake, 5000\n2. Maria, Ancient Forrest, 4500\n3. Celes, Ancient Forrest, 4000\n4. Alexander, Hullbreaker Island, 3500\n5. Kain, Sulphur Lake, 3000\n6. Locke, Hullbreaker Island, 2500\n7. Faris, Ancient Forrest, 2000\n8. Firion, Sulphur Lake, 1500\n9. Gua, Ancient Forrest, 1000\n10. GoGo, Ancient Forrset, 1000\n\nPress Enter to return to the Main Menu...";
}