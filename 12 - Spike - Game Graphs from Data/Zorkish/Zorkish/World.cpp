#include "World.h"

void World::ReadWorldInformation()
{
	ifstream in("TestWorld.txt", ios::in);
	string name, desc, dir, target;
	bool connections = false;
	while (!in.eof())
	{
		if (!connections)
		{
			getline(in, name, ':');
			getline(in, desc, '\n');
			AddLocation(name, desc);

			if ((char)in.peek() == '\n')
			{
				connections = !connections;
				in.ignore();
			}
		}
		else
		{
			getline(in, name, ':');
			getline(in, dir, ':');
			getline(in, target, '\n');
			AddDirection(name, dir, target);
		}
	}
	in.close();
}

void World::ReadWorldInformation(string file)
{
	ifstream in(file, ios::in);
	string name, desc, dir, target;
	bool connections = false;
	while (!in.eof())
	{
		if (!connections)
		{
			getline(in, name, ':');
			getline(in, desc, '\n');
			AddLocation(name, desc);

			if ((char)in.peek() == '\n')
			{
				connections = !connections;
				in.ignore();
			}
		}
		else
		{
			getline(in, name, ':');
			getline(in, dir, ':');
			getline(in, target, '\n');
			AddDirection(name, dir, target);
		}
	}
	in.close();
}

void World::AddLocation(string lName, string lDesc)
{
	locations.push_back(Location(lName, lDesc));
}

void World::AddDirection(string lName, string dir, string target)
{
	Location* t = new Location();
	Location* r = new Location();
	for(int i = 0; i < locations.size(); i++)
	{

		if (locations[i].GetName() == target)
			t = &locations[i];
		
		if (locations[i].GetName() == lName)
		{
			r = &locations[i];
		}
	}

	if (t->GetName().length() != 0 && r->GetName().length() != 0)
		r->AddConnection(dir, t);
	else
	{
		if (t->GetName().length() == 0)
			cout << "target doesn't exist\n";
		else if (r->GetName().length() == 0)
			cout << "location doesn't exist\n";
	}
}

Location* World::GetStartingLocation()
{
	return &locations[0]; //hard coded test - for real files the start will be indicated with the name of "start"
}