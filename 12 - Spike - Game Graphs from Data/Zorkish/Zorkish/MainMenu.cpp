#include "MainMenu.h"
#include "SelectAdventure.h"
#include "HallOfFame.h"
#include "Help.h"
#include "About.h"
#include <iostream>
#include <string>

MainMenu::MainMenu(GameManager* manager) :State(manager) 
{
	breakLoop = false;
}

void MainMenu::GameLoop()
{
	Render();

	while (!breakLoop)
	{
		Update();
		if(!breakLoop)
			Render();
	}
}

void MainMenu::Update()
{
	bool inputGood = false;
	char input;

	while (!inputGood)
	{
		std::cin >> input;

		switch (input)
		{
		case '1': //select adventure
			input = ' ';
			gm->SetState(new SelectAdventure(gm));
			gm->StartStateLoop();
			inputGood = true;
			break;
		case '2': //hall of fame
			input = ' ';
			gm->SetState(new HallOfFame(gm));
			gm->StartStateLoop();
			inputGood = true;
			break;
		case '3': //help
			input = ' ';
			gm->SetState(new Help(gm));
			gm->StartStateLoop();
			inputGood = true;
			break;
		case '4': //about
			input = ' ';
			gm->SetState(new About(gm));
			gm->StartStateLoop();
			inputGood = true;
			break;
		case '5': //quit
			inputGood = true;
			breakLoop = true;
			break;
		default:
			break;
		}
	}
}

void MainMenu::Render()
{
	std::cout << "Welcome to Zorkish Adventures\n\n1. Select an Adventure\n2. Hall of Fame\n3. Help\n4. About\n5. Quit\n\nSelect 1-5:>";;
}