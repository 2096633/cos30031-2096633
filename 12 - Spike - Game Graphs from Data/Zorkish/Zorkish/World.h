#pragma once
#include <vector>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include "Location.h"

#ifndef WORLD_H
#define WORLD_H
#endif


using namespace std;

class World
{
private:
	vector<Location> locations;
public:
	void ReadWorldInformation();
	void ReadWorldInformation(string file);
	void AddLocation(string lName, string lDesc);
	void AddDirection(string lName, string dir, string target);
	Location* GetStartingLocation();
};