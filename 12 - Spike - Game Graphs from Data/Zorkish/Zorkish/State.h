#pragma once
#include "GameManager.h"
#include <iostream>

#ifndef STATE_H
#define STATE_H
#endif

class GameManager;

class State
{
protected:
	GameManager * gm;
public:
	State(GameManager* manager);

	virtual void GameLoop() = 0;
	virtual void Update() = 0;
	virtual void Render() = 0;
};
