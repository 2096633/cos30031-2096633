#include "Player.h"
#include <iostream>

Inventory* Player::GetInventory()
{
	return &inventory;
}

void Player::SetLocation(Location* l)
{
	currentLocation = l;
}

Location* Player::GetLocation()
{
	return currentLocation;
}
