#pragma once
#include "State.h"

class HallOfFame: public State
{
public:
	HallOfFame(GameManager *manager);
	void GameLoop();
	void Update();
	void Render();
};