#include "HighScore.h"
#include "HallOfFame.h"

HighScore::HighScore(GameManager* manager) :State(manager) {}

void HighScore::GameLoop()
{
	Render();
	Update();
}

void HighScore::Update()
{
	std::cin.get();
	std::cin.get();
	gm->SetState(new HallOfFame(gm));
	gm->StartStateLoop();
}

void HighScore::Render()
{
	std::cout << "Congratulations!\n\nYou have made it to Zorkish Hall of Fame\n\nAdventure: [adventure name]\nScore: [player score]\nMoves: [number of moves]\n\nPlease type your name and press enter:\n:>";
}