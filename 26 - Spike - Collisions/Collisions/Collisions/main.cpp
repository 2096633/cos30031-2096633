#include <SDL.h>
#undef main;
#include <iostream>

using namespace std;

static int SCREEN_HEIGHT = 720;
static int SCREEN_WIDTH = 1280;

struct Circle
{
	int x, y;
	int r;
};

void Move(SDL_Rect&);
bool CheckCollision(SDL_Rect& a, SDL_Rect& b);
bool CheckCollision(Circle& a, Circle& b);

int main()
{
	bool quit = false;
	bool box = false;
	bool circle = false;

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		cerr << "SDL failed to load: " << SDL_GetError() << endl;
	}

	SDL_Event _event;

	SDL_Window* _window = SDL_CreateWindow("Collisions", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
	SDL_Renderer* _renderer = SDL_CreateRenderer(_window, -1, 0);
	SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);

	SDL_Surface* bA = SDL_LoadBMP("boxA.bmp");
	SDL_Texture* boxA = SDL_CreateTextureFromSurface(_renderer, bA);
	SDL_Surface* bB = SDL_LoadBMP("boxB.bmp");
	SDL_Texture* boxB = SDL_CreateTextureFromSurface(_renderer, bB);
	SDL_Surface* cA = SDL_LoadBMP("circleA.bmp");
	SDL_Texture* circleA = SDL_CreateTextureFromSurface(_renderer, cA);
	SDL_Surface* cB = SDL_LoadBMP("circleB.bmp");
	SDL_Texture* circleB = SDL_CreateTextureFromSurface(_renderer, cB);

	SDL_Rect boxAR;
	SDL_Rect boxBR;
	SDL_Rect circleAR;
	SDL_Rect circleBR;

	boxAR.x = boxAR.y = boxBR.x = boxBR.y = circleAR.x = circleAR.y = circleBR.x = circleBR.y = 0;
	boxAR.w = boxAR.h = boxBR.w = boxBR.h = circleAR.w = circleAR.h = circleBR.w = circleBR.h = 100;

	SDL_Rect boxAPos;
	SDL_Rect boxBPos;
	SDL_Rect circleAPos;
	SDL_Rect circleBPos;
	Circle a;
	Circle b;

	boxAPos.x = 200;
	boxAPos.y = 200;
	boxBPos.x = 50;
	boxBPos.y = 50;
	circleAPos.x = 200;
	circleAPos.y = 200;
	circleBPos.x = 50;
	circleBPos.y = 50;
	boxAPos.w = boxAPos.h = boxBPos.w = boxBPos.h = circleAPos.w = circleAPos.h = circleBPos.w = circleBPos.h = 100;

	SDL_RenderCopy(_renderer, NULL, NULL, NULL);

	while (!quit)
	{
		SDL_Delay(20);
		SDL_PollEvent(&_event);

		switch (_event.type)
		{
		case SDL_QUIT:
			quit = !quit;
			break;
		case SDL_KEYDOWN:
			switch (_event.key.keysym.sym)
			{
			case SDLK_1: //box collision
				box = !box;
				if (box)
					circle = false;
				boxAPos.x = 200;
				boxAPos.y = 200;
				boxBPos.x = 50;
				boxBPos.y = 50;
				
				break;
			case SDLK_2: //circle collision
				circle = !circle;
				if (circle)
					box = false;
				circleAPos.x = 200;
				circleAPos.y = 200;
				circleBPos.x = 50;
				circleBPos.y = 50;
				break;
			}
		}

		SDL_RenderClear(_renderer);

		if(box)
		{
			SDL_RenderCopy(_renderer, boxA, &boxAR, &boxAPos);
			SDL_RenderCopy(_renderer, boxB, &boxBR, &boxBPos);
			Move(boxBPos);
			if (CheckCollision(boxAPos, boxBPos))
				cout << "Boxes have collided" << endl;
		}

		if (circle)
		{
			SDL_RenderCopy(_renderer, circleA, &circleAR, &circleAPos);
			SDL_RenderCopy(_renderer, circleB, &circleBR, &circleBPos);
			Move(circleBPos);
			a = {circleAPos.x, circleAPos.y, circleAPos.h / 2};
			b = {circleBPos.x, circleBPos.y, circleBPos.h /2};
			if (CheckCollision(a, b))
				cout << "Circles have collided" << endl;
		}

		SDL_RenderPresent(_renderer);
	}

	SDL_DestroyRenderer(_renderer);
	SDL_DestroyWindow(_window);
	SDL_Quit();
	return 0;
}

void Move(SDL_Rect& m)
{
	m.x += 1;

	if ((m.x < 0) || (m.x + m.w > SCREEN_WIDTH))
		m.x -= 1;

	m.y += 1;

	if ((m.y < 0) || (m.y + m.h > SCREEN_HEIGHT))
		m.y -= 1;
}


bool CheckCollision(SDL_Rect& a, SDL_Rect& b)
{
	int leftA, leftB, rightA, rightB, topA, topB, bottomA, bottomB;

	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	if (bottomA <= topB)
		return false;

	if (topA >= bottomB)
		return false;

	if (rightA <= leftB)
		return false;

	if (leftA >= rightB)
		return false;

	return true;
}

bool CheckCollision(Circle& a, Circle& b)
{
	int totalRadius = a.r + b.r;
	totalRadius = totalRadius * totalRadius;

	double distance = ((b.x - a.x) * (b.x - a.x)) + ((b.y - a.y) * (b.y - a.y));

	if (distance < totalRadius)
		return true;

	return false;
}
