#include "Binary.h"
#include <iostream>
#include <fstream>

using namespace std;

Binary::Binary()
{
	i = 0;
	c = ' ';
	f = 0;
}

void Binary::ShowValues()
{
	cout << i << endl << c << endl << f << endl;
	//cout << sizeof(i) << endl << sizeof(c) << endl << sizeof(f) << endl;
}

void Binary::OutputBinary()
{
	ofstream out;
	out.open("test1.bin", ios::out | ios::binary);
	out.write((char*)&i, sizeof(i));
	out.write((char*)&c, sizeof(c));
	out.write(reinterpret_cast<const char*>(&f), sizeof(f));
	out.close();
}

void Binary::ReadBinary()
{
	ifstream in;
	in.open("test1.bin", ios::in | ios::binary);
	in.read((char*)&i, sizeof(i));
	in.read((char*)&c, sizeof(c));
	in.read((char*)&f, sizeof(f));
	in.close();
	ShowValues();
}