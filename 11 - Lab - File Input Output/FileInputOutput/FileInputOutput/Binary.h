#pragma once

class Binary
{
private:
	int i;
	char c;
	float f;
public:
	Binary();
	void ShowValues();
	void OutputBinary();
	void ReadBinary();
};
