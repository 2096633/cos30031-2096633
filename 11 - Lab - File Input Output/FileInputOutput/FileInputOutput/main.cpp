#include "Binary.h"
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include "json.hpp"

using namespace std;
using json = nlohmann::json;

int main()
{
	//Binary - part 1
	//Binary b;
	//b.ShowValues();
	//b.OutputBinary();
	//5, g, 3.145
	//b.ReadBinary();

	//Reading from txt file and splitting
	/*string line;
	ifstream in;
	in.open("test2.txt", ios::in);
	while (!in.eof())
	{
		getline(in, line);

		while (line.length() == 0)
		{
			while (getline(in, line, ':'))
			{
				if (line[0] != '#')
				{
					cout << line << endl;
				}
			}
		}
	}*/

	//JSON file
	ifstream i("test3.json", ios::in);
	json j;
	i >> j;
	i.close();
	cout << setw(4) << j << endl;

	return 0;
}