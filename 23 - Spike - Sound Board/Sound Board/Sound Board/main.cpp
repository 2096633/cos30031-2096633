#include <iostream>
#include <SDL.h>
#include <SDL_mixer.h>

//all sounds came from www.sampleswap.org

using namespace std;

int main(int argc, char* argv[])
{
	
	bool quit = false;
	SDL_Event _event;
	static const char* moo1 = "moo.wav";
	static const char* moo2 = "mooalarmed.wav";
	static const char* moo3 = "moochorus.wav";
	static const char* music = "true.wav";
	
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		cout << "SLD_AUDIO could not be initialised. SDL error: " << SDL_GetError() << endl;
		return 1;
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) < 0)
		cerr << "Error initialising Mixer" << endl;

	Mix_AllocateChannels(32);

	Mix_Chunk* m1 = Mix_LoadWAV(moo1);
	Mix_Chunk* m2 = Mix_LoadWAV(moo2);
	Mix_Chunk* m3 = Mix_LoadWAV(moo3);
	Mix_Music* Music = Mix_LoadMUS(music);
	
	if (m1 == NULL)
	{
		cerr << "Failed to load moo1" << endl;
		return 1;
	}
	if (m2 == NULL)
	{
		cerr << "Failed to load moo2" << endl;
		return 1;
	}
	if (m3 == NULL)
	{
		cerr << "Failed to load moo3" << endl;
		return 1;
	}
	if (Music == NULL)
	{
		cerr << "Failed to load music" << endl;
		return 1;
	}

	SDL_Window* window = SDL_CreateWindow("Moo Bored", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 300, 100, 0);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

	while (!quit)
	{
		SDL_Delay(20);
		SDL_PollEvent(&_event);

		switch (_event.type)
		{
		case SDL_QUIT:
			quit = true;
			break;
		case SDL_KEYDOWN:
			switch (_event.key.keysym.sym)
			{
			case SDLK_1:
				cout << "playing sound 1" << endl;
				Mix_PlayChannel(-1, m1, NULL);
				break;
			case SDLK_2:
				cout << "playing sound 2" << endl;
				Mix_PlayChannel(-1, m2, NULL);
				break;
			case SDLK_3:
				cout << "playing sound 3" << endl;
				Mix_PlayChannel(-1, m3, NULL);
				break;
			case SDLK_0:
				cout << "toggle BGM: ";
				if (Mix_PlayingMusic() == 0)
				{
					Mix_PlayMusic(Music, -1);
					cout << "on" << endl;
				}
				else
				{
					if (Mix_PausedMusic() == 1)
					{
						Mix_ResumeMusic();
						cout << "on" << endl;
					}
					else
					{
						Mix_PauseMusic();
						cout << "off" << endl;
					}
				}
				break;
			}
		}
	}
	
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
