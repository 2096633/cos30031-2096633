// GridWorld.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <istream>
#include <thread>
#include <ctime>

using namespace std;

void Input();
void Update();
void Render();
void Timer();

//Hard coded map
char map[8][8] = {
{'#','#','#','#','#','#','#','#'},
{'#','G',' ','D','#','D',' ','#'},
{'#',' ',' ',' ','#',' ',' ','#'},
{'#','#','#',' ','#',' ','D','#'},
{'#',' ',' ',' ','#',' ',' ','#'},
{'#',' ','#','#','#','#',' ','#'},
{'#',' ',' ',' ',' ',' ',' ','#'},
{'#','#','S','#','#','#','#','#'} };
//Game over bool for while game loop
bool gameOver = false;
//Bool to check if input has been entered, can proceed to rendering
bool inputReady = true;
bool updated = true;

//Player position variables (defaults to S on map)
int x = 7, y = 2;

//Input variable for thread
char playerInput;

//Timer varaibles
int start = clock();
double _time;

int main()
{
	//intro print
	cout << "Welcome to GridWorld: Quantised Excitement. Fate is waiting for you!\nValid commands: N, S, E and W for direction. Q to quit the game.\nYou have 60 seconds left until GridWorld is filled with lava!\n";
	Render();
	//start seperate thread for the timer and input
	thread tTimer(Timer);
	thread tInput(Input);
	//game loop
	while (!gameOver)
	{
			Update();
			Render();
	}
	//join the timer thread back with the main thread
	tTimer.join();
	tInput.join();
	if (map[x][y] == 'D')
		cout << "You have fallen into a pit.\nYou are ded!\nBetter luck next time!\n";
	else if (map[x][y] == 'G')
		cout << "You found the lost chest of gold.\nYou win!\nThanks for playing!\n";
	else if (playerInput == 'q')
		cout << "You have quit.\nCome back when you think you're ready!\n";
	else
		cout << "You were too slow!\nThe lava consumed you " << (int)abs(_time) - 60 << " seconds ago!\nBetter luck next time!\n";
    return 0;
}

//Check valid moves for player
bool CheckMove(int x, int y)
{
	if (x > 7 || y > 7) //out of range exception
		return false;

	if (map[x][y] == '#') //your not a ghost, no walking through walls
		return false;
	else //Either white space or gold or maybe ded?
		return true;

}

//Check valid moves for player
void ValidMoves()
{
	cout << "You can move: ";
	if (map[x - 1][y] != '#')
		cout << "N ";
	if (map[x][y + 1] != '#')
		cout << "E ";
	if (map[x + 1][y] != '#' && (x + 1) < 8)
		cout << "S ";
	if (map[x][y - 1] != '#')
		cout << "W ";
	cout << ">";
}

void Input()
{
	while(!gameOver)
	{
		if (inputReady)
		{
			cout << "You have " << 60 - (int)_time << " seconds left\n";
			ValidMoves();
			cin >> playerInput;
			inputReady = false;
		}
	}
}

//Update the map
void Update()
{
	if (!gameOver && playerInput != ' ' && !inputReady)
	{
		bool goodMove = false;
		while (!goodMove) //while player doesn't enter a valid move option keep looping until input is deemed valid
		{
			switch (tolower(playerInput)) //convert input to lower case
			{
			case('n'):
				if (CheckMove(x - 1, y))
					x--;
				break;
			case('e'):
				if (CheckMove(x, y + 1))
					y++;
				break;
			case('s'):
				if (CheckMove(x + 1, y))
					x++;
				break;
			case('w'):
				if (CheckMove(x, y - 1))
					y--;
				break;
			case('q'):
				gameOver = true;
				break;
			default:
				ValidMoves();
			}

			//check if a valid move has been entered
			goodMove = CheckMove(x, y);
			//check if move lands you on the gold or death
			if (goodMove)
			{
				if (map[x][y] == 'D' || map[x][y] == 'G') //you either good or ded
					gameOver = !gameOver;
			}
		}
		if (_time >= 60)
			gameOver = true;
		updated = true;
	}
}

//Render out map and player position
void Render()
{
	if(!gameOver && updated)
	{
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				if (i == x && j == y)
					cout << 'P';
				else
					cout << map[i][j];
			}
			cout << endl;
		}
		inputReady = true;
		updated = false;
	}
}

void Timer()
{
	while (1)
	{
		_time = (clock() - start) / (double)(CLOCKS_PER_SEC);
		if (gameOver)
			break;
	}
}
