#include <iostream>
#include <SDL.h>

using namespace std;

static int SCREEN_WIDTH = 1280;
static int SCREEN_HEIGHT = 720;

int RandHeight();
int RandWidth();

int main(int argc, char* argv[])
{
	bool quit = false;
	bool bgRender = true;
	bool s1Render = false;
	bool s2Render = false;
	bool s3Render = false;
	SDL_Event _event;

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		cerr << "Failed to load SDL. SDL error : " << SDL_GetError() << endl;
		return 1;
	}
	
	SDL_Window* _window = SDL_CreateWindow("Sprites and Graphics", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
	SDL_Renderer* _renderer = SDL_CreateRenderer(_window, -1, 0);

	SDL_Surface* image = SDL_LoadBMP("test.bmp");
	SDL_Texture* tex = SDL_CreateTextureFromSurface(_renderer, image);

	SDL_Surface* spritesheet = SDL_LoadBMP("sprites.bmp");
	SDL_Texture* sTex = SDL_CreateTextureFromSurface(_renderer, spritesheet);

	SDL_Rect s1;
	SDL_Rect s2;
	SDL_Rect s3;
	s1.h = s1.w = s2.h = s2.w = s3.h = s3.w = 100;
	s1.x = s1.y = s2.y = s3.y = 0;
	s2.x = 100;
	s3.x = 200;

	SDL_Rect s1Pos;
	SDL_Rect s2Pos;
	SDL_Rect s3Pos;

	s1Pos.h = s1Pos.w = s2Pos.h = s2Pos.w = s3Pos.h = s3Pos.w = 100;
	s1Pos.x = s1Pos.y = s2Pos.x = s2Pos.y = s3Pos.x = s3Pos.y = 0;

	SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
	SDL_RenderCopy(_renderer, tex, NULL, NULL);

	while (!quit)
	{
		SDL_Delay(20);
		SDL_PollEvent(&_event);

		switch (_event.type)
		{
		case SDL_QUIT:
			quit = !quit;
			break;
		case SDL_KEYDOWN:
			switch (_event.key.keysym.sym)
			{
			case SDLK_1:
				s1Render = !s1Render;
				if (s1Render)
				{
					s1Pos.x = RandWidth();
					s1Pos.y = RandHeight();

					while ((s1Pos.x <= (s2Pos.x + s2Pos.w) && (s1Pos.x + s1Pos.w) >= s2Pos.x) || (s1Pos.x <= (s3Pos.x + s3Pos.w) && (s1Pos.x + s1Pos.w) >= s3Pos.x))
						s1Pos.x = RandWidth();
					while ((s1Pos.y <= (s2Pos.y + s2Pos.h) && (s1Pos.y + s1Pos.h) >= s2Pos.y) || (s1Pos.y <= (s3Pos.y + s3Pos.h) && (s1Pos.y + s1Pos.h) >= s3Pos.y))
						s1Pos.y = RandHeight();
				}
				cout << "Sprite 1: " << boolalpha << s1Render << " x: " << s1Pos.x << " y: " << s1Pos.y << endl;
				break;
			case SDLK_2:
				s2Render = !s2Render;
				if (s2Render)
				{
					s2Pos.x = RandWidth();
					s2Pos.y = RandHeight();

					while ((s2Pos.x <= (s1Pos.x + s1Pos.w) && (s2Pos.x + s2Pos.w) >= s1Pos.x) || (s2Pos.x <= (s3Pos.x + s3Pos.w) && (s2Pos.x + s2Pos.w) >= s3Pos.x))
						s2Pos.x = RandWidth();
					while ((s2Pos.y <= (s1Pos.y + s1Pos.h) && (s2Pos.y + s2Pos.h) >= s1Pos.y) || (s2Pos.y <= (s3Pos.y + s3Pos.h) && (s2Pos.y + s2Pos.h) >= s3Pos.y))
						s2Pos.y = RandHeight();
				}
				cout << "Sprite 2: " << boolalpha << s2Render << " x: " << s2Pos.x << " y: " << s2Pos.y << endl;
				break;
			case SDLK_3:
				s3Render = !s3Render;
				if (s3Render)
				{
					s3Pos.x = RandWidth();
					s3Pos.y = RandHeight();

					while ((s3Pos.x <= (s2Pos.x + s2Pos.w) && (s3Pos.x + s3Pos.w) >= s2Pos.x) || (s3Pos.x <= (s1Pos.x + s1Pos.w) && (s3Pos.x + s3Pos.w) >= s1Pos.x))
						s3Pos.x = RandWidth();
					while ((s3Pos.y <= (s2Pos.y + s2Pos.h) && (s3Pos.y + s3Pos.h) >= s2Pos.y) || (s3Pos.y <= (s1Pos.y + s1Pos.h) && (s3Pos.y + s3Pos.h) >= s1Pos.y))
						s3Pos.y = RandHeight();
				}
				cout << "Sprite 3: " << boolalpha << s3Render << " x: " << s3Pos.x << " y: " << s3Pos.y << endl;
				break;
			case SDLK_0:
				bgRender = !bgRender;
				cout << "Background: " << boolalpha << bgRender << endl;
				break;
			}
		}

		SDL_RenderClear(_renderer);

		if (bgRender)
			SDL_RenderCopy(_renderer, tex, NULL, NULL);

		if(s1Render)
			SDL_RenderCopy(_renderer, sTex, &s1, &s1Pos);

		if(s2Render)
			SDL_RenderCopy(_renderer, sTex, &s2, &s2Pos);

		if(s3Render)
			SDL_RenderCopy(_renderer, sTex, &s3, &s3Pos);


		SDL_RenderPresent(_renderer);
	}

	SDL_FreeSurface(image);
	SDL_DestroyRenderer(_renderer);
	SDL_DestroyWindow(_window);
	SDL_Quit();
	return 0;
}

int RandHeight()
{
	return rand() % (SCREEN_HEIGHT - 100) + 1;
}

int RandWidth()
{
	return rand() % (SCREEN_WIDTH - 100) + 1;
}