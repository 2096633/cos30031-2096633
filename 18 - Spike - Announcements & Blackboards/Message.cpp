#include "Message.h"

template<class T>
Message<T>::Message(GameEntity s, GameEntity t, std::string m, T d)
{
	sender = s;
	target = t;
	message = m;
	data = d;
}

template<class T>
GameEntity* Message<T>::GetSender()
{
	return sender;
}

template<class T>
GameEntity* Message<T>::GetTarget()
{
	return target;
}

template<class T>
std::string Message<T>::GetMessage()
{
	return message;
}

template<class T>
T Message<T>::GetData()
{
	return data;
}
