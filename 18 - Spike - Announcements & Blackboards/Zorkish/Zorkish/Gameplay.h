#pragma once
#include "State.h"
#include "Player.h" 
#include "World.h"
#include "CommandManager.h"
#include "MessageHandler.h"
#include <string>

class Gameplay : public State
{
private:
	bool breakLoop;
	Player p;
	World gameWorld;
	vector<string> moves;
	CommandManager cm;
	MessageHandler mh;
public:
	Gameplay(GameManager *gm);
	void GameLoop();
	void SetupGame();
	void Update();
	void Render();
	void PossibleMoves();
};