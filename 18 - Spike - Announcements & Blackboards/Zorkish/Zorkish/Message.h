#pragma once
#include "Item.h"
#include <string>

class Item;

class Message
{
public:
	Message(GameEntity* sender, GameEntity* target, std::string message, Item* i);
	Message(GameEntity* sender, GameEntity* target, std::string message, int n);
	GameEntity* GetSender();
	GameEntity* GetTarget();
	std::string GetMessage();
	Item* GetItem();
	int GetNumber();
private:
	GameEntity* sender;
	GameEntity* target;
	std::string message;
	Item* item;
	int number;
};
