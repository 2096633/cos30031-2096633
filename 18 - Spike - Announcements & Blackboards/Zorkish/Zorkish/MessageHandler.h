#pragma once
#include "Message.h"
#include "Player.h"
#include <vector>

class MessageHandler
{
public:
	MessageHandler();
	void SendMessage(Message);
	void DispatchMessages();
	void ChangeLocation(Player*);
private:
	std::vector<GameEntity*> entities;
	std::vector<Message> messages;
};
