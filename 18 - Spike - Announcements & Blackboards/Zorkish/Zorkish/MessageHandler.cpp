#include "MessageHandler.h"

MessageHandler::MessageHandler(){}

void MessageHandler::SendMessage(Message m)
{
	messages.push_back(m);
}

void MessageHandler::DispatchMessages()
{
	//TO-DO find targets in entities and if no nullptr send message
	for (int i = 0; i < messages.size(); i++)
	{
		for (int j = 0; j < entities.size(); j++)
		{
			if (messages[i].GetTarget() == entities[j])
			{
				Player * p = static_cast<Player*>(entities[j]);
				Enemy * e = static_cast<Enemy*>(entities[j]);
				Door * d = static_cast<Door*>(entities[j]);
				switch (entities[j]->GetType())
				{
				case player:
					p->SendMessage(messages[i]);
					break;
				case enemy:
					e->SendMessage(messages[i]);
					break;
				case door:
					d->SendMessage(messages[i]);
					break;
				default:
					break;
				}
			}
		}
	}
	messages.clear();
}

void MessageHandler::ChangeLocation(Player* p)
{
	entities.clear();
	entities.push_back(p);
	for (int i = 0; i < p->GetLocation()->GetEnemies()->size(); i++)
		entities.push_back(&p->GetLocation()->GetEnemies()->operator[](i));
	for (int i = 0; i < p->GetLocation()->GetDoors()->size(); i++)
		entities.push_back(&p->GetLocation()->GetDoors()->operator[](i));
}
