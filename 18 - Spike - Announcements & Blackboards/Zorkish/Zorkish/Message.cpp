#include "Message.h"

Message::Message(GameEntity * s, GameEntity * t, std::string m, Item* i)
{
	sender = s;
	target = t;
	message = m;
	item = i;
	number = 0;
}

Message::Message(GameEntity * s, GameEntity * t, std::string m, int n)
{
	sender = s;
	target = t;
	message = m;
	number = n;
	item = nullptr;
}

GameEntity * Message::GetSender()
{
	return sender;
}

GameEntity * Message::GetTarget()
{
	return target;
}

std::string Message::GetMessage()
{
	return message;
}

Item* Message::GetItem()
{
	return item;
}

int Message::GetNumber()
{
	if (number != 0)
		return number;
	return 0;
}
