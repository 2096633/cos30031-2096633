#include "HelpCommand.h"
#include <iostream>

void HelpCommand::Execute()
{
	std::cout << "The following commands are supported:\n-quit\n-go _\n-look\n-look at _\n-inventory\n-open _ with _\n-close _\n-attack _ with _\n-take _ [from _]\n-put _ in _\n-drop _\n";
}