#pragma once
#include <vector>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include "Location.h"

#ifndef WORLD_H
#define WORLD_H
#endif


using namespace std;

class World
{
private:
	vector<Location> locations;
public:
	void ReadWorldInformation();
	void ReadWorldInformation(string file);
	void AddLocation(string lName, string lDesc);
	void AddDirection(string lName, string dir, string target);
	string GetLocationTree();
	Location* GetStartingLocation();
	void AddItem(string lName, string iName, string iDesc, bool c);
	void AddToContainer(string loc, string cName, string iName, string iDesc);
	void AddEnemy(string loc, string eName, string eDesc, int hp, int dmg);
	void AddDoor(string loc, string dName, string dDesc, string, string , string);
	void AddComponent(string loc, string iName, int dmg);
};