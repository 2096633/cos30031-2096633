#pragma once
//#include "Message.h"
#include <string>
#include <vector>

enum ObjectType {player, enemy, door};

class GameEntity
{
public:
	GameEntity();
	GameEntity(std::string n, std::string d);
	GameEntity(std::string n, std::string d, ObjectType type);
	std::string GetName();
	std::string GetDescription();
	ObjectType GetType();
	//virtual void SendMessage(Message) = 0;
protected:
	std::string name;
	std::string desc;
	ObjectType t;
};
