#include "Help.h"

Help::Help(GameManager* mananger) :State(mananger) {}

void Help::GameLoop()
{
	Render();
	Update();
}

void Help::Update()
{
	std::cin.get();
	std::cin.get();
}

void Help::Render()
{
	std::cout << "The following commands are supported:\n-quit\n-go _\n-look\n-look at _\n-look in _\n-inventory\n-open _ with _\n-close _\n-attack _ with _\n-take _ [from _]\n-put _ in _\n-drop _\n\nPress Enter to return to the Main Menu...";
}