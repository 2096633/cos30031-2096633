#include "Container.h"

Container::Container() :Item("Item not found", "Item not found") 
{
	isC = true; 
}

Container::Container(std::string n, std::string d) : Item(n, d) 
{
	isC = true; 
}

Container::Container(const Container& c)
{
	name = c.name;
	desc = c.desc;
	isC = true;
}

Inventory* Container::GetContents()
{
	return &inv;
}