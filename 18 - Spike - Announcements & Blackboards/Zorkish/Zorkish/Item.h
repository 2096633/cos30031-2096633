#pragma once
#include "GameEntity.h"
#include "Damage.h"
#include "Message.h"

class Message;

class Item : public GameEntity
{
public:
	Item();
	Item(std::string pName, std::string pDesc);
	Item(const Item &i2);
	bool IsContainer();
	Damage* GetDamage();
	void SetDamage(int d);
	bool operator==(const Item& rhs) const;
	void SendMessage(Message);
protected:
	bool isC;
private:
	Damage damage;
};