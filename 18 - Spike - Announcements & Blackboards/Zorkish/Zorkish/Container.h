#pragma once
#include "Inventory.h"
#include "Item.h"
#include "GameEntity.h"
#include <string>

class Container : public Item
{
public:
	Container();
	Container(std::string name, std::string desc);
	Container(const Container& c);
	Inventory* GetContents();
private:
	Inventory inv;
};
