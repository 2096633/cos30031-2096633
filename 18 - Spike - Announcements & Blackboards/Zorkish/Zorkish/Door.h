#pragma once
#include "Lock.h"
#include "Item.h"
#include "Message.h"

class Door : public GameEntity
{
public:
	Door(std::string n, std::string d, std::string dr, Item i);
	bool IsLocked();
	void Unlock(Item k);
	std::string GetDirection();
	void SendMessage(Message m);
private:
	std::string dir;
	Lock lock;
};