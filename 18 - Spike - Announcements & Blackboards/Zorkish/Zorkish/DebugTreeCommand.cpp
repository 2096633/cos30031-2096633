#include "DebugTreeCommand.h"
#include <iostream>

DebugTreeCommand::DebugTreeCommand(Player* p, World* w)
{
	player = p;
	gameWorld = w;
}

void DebugTreeCommand::Execute()
{
	std::cout << gameWorld->GetLocationTree();
}