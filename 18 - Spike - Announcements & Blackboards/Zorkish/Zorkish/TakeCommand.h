#pragma once
#include "Command.h"
#include "Player.h"

class TakeCommand :public Command
{
public:
	TakeCommand(Player* p);
	void Execute();
private:
	Player* player;
};
