#pragma once
#include "GameEntity.h"
#include "Inventory.h"
#include "Location.h"
#include "Health.h"
#include "Message.h"

class Player : public GameEntity
{
private:
	Inventory inventory;
	Location* currentLocation;
	Health health;
	Damage damage;
public:
	Player();
	Inventory* GetInventory();
	void SetLocation(Location* l);
	Location* GetLocation();
	Health* GetHp();
	Damage* GetDamage();
	void SendMessage(Message m);
};
