#pragma once
#include <vector>
#include <string>
#include "Inventory.h"
#include "Door.h"
#include "Enemy.h"

#ifndef LOCATION_H
#define LOCATION_H
#endif

using namespace std;

class Location
{
private:
	struct Connection
	{
		string direction;
		Location* target;
	};
	string name;
	string description;
	vector<Connection> connections;
	vector<Door> doors;
	vector<Enemy> enemies;
	Inventory inv;
public:
	Location();
	Location(string pName, string desc);
	void AddConnection(string dir, Location* target);
	Location* GetConnection(string dir);
	vector<string> PossibleDirections();
	string GetName();
	string GetDescription();
	Inventory* GetInventory();
	vector<Door>* GetDoors();
	Door* GetDoor(string n);
	Door* GetDoorByDir(string dir);
	void AddDoor(Door);
	vector<Enemy>* GetEnemies();
	Enemy* GetEnemy(string n);
	void AddEnemy(Enemy);
	void AddItem(Item* i);
	string GetConnections();
};