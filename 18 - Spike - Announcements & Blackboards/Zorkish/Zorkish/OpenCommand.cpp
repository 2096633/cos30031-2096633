#include "OpenCommand.h"
#include <iostream>

OpenCommand::OpenCommand(Player * p)
{
	player = p;
}

void OpenCommand::Execute()
{
	if (input.size() == 5 && input[2] == "with")
	{
		Door* d = player->GetLocation()->GetDoor(input[1]);
		if (d != nullptr)
		{
			Item* i = player->GetInventory()->GetItem(input[3]);
			d->Unlock(*i);

			if (d->IsLocked())
				cout << "Not even the master of unlocking could open " << input[1] << " with " << input[3] << endl;
			else
				cout << input[1] << " has been unlocked" << endl;
		}
	}
}
