#pragma once

class Health
{
public:
	Health();
	Health(int max);
	int GetCurrentHealth();
	void AdjustHealth(int amt);
private:
	int maxHealth;
	int currentHealth;
};