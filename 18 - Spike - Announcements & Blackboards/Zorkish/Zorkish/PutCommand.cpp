#include "PutCommand.h"
#include "Container.h"
#include <iostream>

PutCommand::PutCommand(Player* p)
{
	player = p;
}

void PutCommand::Execute()
{
	//put [item] in [container]
	Item* i = player->GetInventory()->GetItem(input[1]);
	Item* iP = player->GetInventory()->GetItem(input[3]);
	Item* iL = player->GetLocation()->GetInventory()->GetItem(input[3]);

	if (i->GetName() == input[1])
	{
		if (iP->GetName() == input[3] && iP->IsContainer())
		{
			Container* c = static_cast<Container*>(player->GetInventory()->GetItem(input[3]));
			c->GetContents()->PutItem(i);
			player->GetInventory()->RemoveItem(input[1]);
		}
		else if (iL->GetName() == input[3] && iL->IsContainer())
		{
			Container* c = static_cast<Container*>(player->GetLocation()->GetInventory()->GetItem(input[3]));
			c->GetContents()->PutItem(i);
			player->GetInventory()->RemoveItem(input[1]);
		}
	}
	else
		cout << input[1] << " not found" << endl;
}