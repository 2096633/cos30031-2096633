#include "Enemy.h"
#include <iostream>

Enemy::Enemy(std::string n, std::string d, int h, int dmg) : GameEntity(n, d, enemy)
{
	health = Health(h);
	damage = Damage(dmg);
	if (health.GetCurrentHealth() < 0)
		isDed = false;
	else
		isDed = true;
}

Inventory * Enemy::GetInventory()
{
	if (isDed)
		return &inv;
	else
		return nullptr;
}

Health * Enemy::GetHp()
{
	return &health;
}

Damage * Enemy::GetDPS()
{
	return &damage;
}

bool Enemy::IsDed()
{
	return isDed;
}

void Enemy::SendMessage(Message m)
{
	if (m.GetMessage() == "attack")
	{
		if (isDed)
			std::cout << name << " is already dead!" << std::endl;
		health.AdjustHealth(-m.GetNumber());
		std::cout << m.GetSender()->GetName() << " has done " << m.GetNumber() << " damage to " << name << std::endl;
		if (health.GetCurrentHealth() < 0)
		{
			isDed = true;
			std::cout << name << " has been vanquished" << std::endl;
		}
	}
}