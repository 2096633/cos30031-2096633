#pragma once
#include "Item.h"

class Lock
{
public:
	Lock();
	Lock(Item);
	bool IsLocked();
	void Unlock(Item);
private:
	bool locked;
	Item key;
};
