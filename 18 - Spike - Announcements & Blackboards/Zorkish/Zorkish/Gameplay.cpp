#include "Gameplay.h"
#include "HighScore.h"
#include <string>
#include <algorithm>
#include <sstream>

Gameplay::Gameplay(GameManager* manager) :State(manager)
{ 
	breakLoop = false;
	cm = CommandManager(&p, &gameWorld);
}

void Gameplay::GameLoop()
{
	SetupGame();

	while (!breakLoop)
	{
		Update();
		if(!breakLoop)
			Render();
	}
}

void Gameplay::SetupGame()
{
	std::cout << "Welcome to Zorkish: Test Land\nThere are now some locations. Please explore!\n";
	gameWorld.ReadWorldInformation();
	p.SetLocation(gameWorld.GetStartingLocation());
	moves = p.GetLocation()->PossibleDirections();
	PossibleMoves();
	cout << ":>";
}

void Gameplay::Update()
{
	string input;
	bool inputGood = false;

	mh.ChangeLocation(&p);

	while (!inputGood)
	{
		cin.clear();
		getline(cin, input);
		transform(input.begin(), input.end(), input.begin(), ::tolower);

		vector<string> in;
		stringstream iss(input);
		while (iss)
		{
			string o;
			iss >> o;
			transform(o.begin(), o.end(), o.begin(), ::tolower);
			in.push_back(o);
		}

		if (in[0] == "attack")
		{
			if (in.size() == 3)
			{
				Enemy* t = p.GetLocation()->GetEnemy(in[1]);
				if (t != nullptr)
					mh.SendMessage(Message(&p, t, "attack", p.GetDamage()->GetDamage()));
				inputGood = !inputGood;
			}
			else if (in.size() > 3 && in[2] == "with")
			{
				Enemy* t = p.GetLocation()->GetEnemy(in[1]);
				int iDmg = 0;
				if(p.GetInventory()->GetItem(in[3])->GetName() == in[3])
					iDmg = p.GetInventory()->GetItem(in[3])->GetDamage()->GetDamage();
				if (t != nullptr)
					mh.SendMessage(Message(&p, t, "attack", iDmg));
				inputGood = !inputGood;
			}
		}
		else if (in[0] == "go")
		{
			transform(input.begin(), input.end(), input.begin(), ::tolower);
			Location* here = p.GetLocation();

			if (in[1] == "n")
				in[1] = "north";
			else if (in[1] == "e")
				in[1] = "east";
			else if (in[1] == "s")
				in[1] = "south";
			else if (in[1] == "w")
				in[1] = "west";
			else if (in[1] == "ne")
				in[1] = "northeast";
			else if (in[1] == "se")
				in[1] == "southeast";
			else if (in[1] == "sw")
				in[1] = "southwest";
			else if (in[1] == "nw")
				in[1] = "northwest";
			else if (in[1] == "u")
				in[1] = "up";
			else if (in[1] == "d")
				in[1] = "down";

			if (here->GetDoorByDir(in[1]) != nullptr)
			{
				if (here->GetDoorByDir(in[1])->IsLocked())
				{
					std::cout << "The door is locked, you'll have to find another way\n";
					inputGood = !inputGood;
				}
				else
				{
					for (int i = 0; i < moves.size(); i++)
					{
						if (moves[i] == in[1])
						{
							p.SetLocation(p.GetLocation()->GetConnection(in[1]));
							cout << "You move " << in[1] << endl;
							inputGood = !inputGood;
						}
					}
					if (here == p.GetLocation())
						std::cout << "There isn't a path there.\n:>";
				}
			}
			else
			{
				for (int i = 0; i < moves.size(); i++)
				{
					if (moves[i] == in[1])
					{
						p.SetLocation(p.GetLocation()->GetConnection(in[1]));
						inputGood = !inputGood;
					}
				}
				if (here == p.GetLocation())
					std::cout << "There isn't a path there.\n:>";
			}
		}
		else if (input == "quit")
		{
			inputGood = !inputGood;
			breakLoop = true;
		}
		else if (input == "hiscore")
		{
			inputGood = !inputGood;
			breakLoop = true;
			gm->SetState(new HighScore(gm));
			gm->StartStateLoop();
		}
		else if (input != "")
		{
			cm.PickCommand(input);
			inputGood = !inputGood;
		}

	}

	moves = p.GetLocation()->PossibleDirections();
	mh.DispatchMessages();
}

void Gameplay::Render()
{
	PossibleMoves();
	if (p.GetLocation()->GetEnemies()->size() > 0)
	{
		vector<Enemy> e = *p.GetLocation()->GetEnemies();
		for (int i = 0; i < e.size(); i++)
		{
			cout << "A " << e[i].GetName() << " has appeared! " << e[i].GetDescription() << endl;
		}
	}
	cout << ":>";
}

void Gameplay::PossibleMoves()
{
	std::cout << "You can move: ";

	for (int i = 0; i < moves.size(); i++)
	{
		if (i == (moves.size() - 1))
			std::cout << moves[i] << endl;
		else
			std::cout << moves[i] << ", ";
	}
}