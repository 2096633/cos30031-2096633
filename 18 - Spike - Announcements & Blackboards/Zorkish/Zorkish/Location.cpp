#include "Location.h"
#include <iostream>

Location::Location() {}

Location::Location(string pName, string desc)
{
	name = pName;
	description = desc;
}

void Location::AddConnection(string dir, Location * target)
{
	connections.push_back(Connection{dir, target});
}

Location* Location::GetConnection(string dir)
{
	Location* l = new Location();

	for (int i = 0; i < connections.size(); i++)
	{
		if (connections[i].direction == dir)
			l = connections[i].target;
	}
	return l;
}

vector<string> Location::PossibleDirections()
{
	vector<string> result;

	for (int i = 0; i < connections.size(); i++)
		result.push_back(connections[i].direction);

	return result;
}

string Location::GetName()
{
	return name;
}

string Location::GetDescription()
{
	return description;
}

Inventory* Location::GetInventory()
{
	return &inv;
}

vector<Door>* Location::GetDoors()
{
	return &doors;
}

Door* Location::GetDoor(string n)
{
	Door* d = nullptr;
	for (int i = 0; i < doors.size(); i++)
		if (doors[i].GetName() == n)
			d = &doors[i];
	return d;
}

Door * Location::GetDoorByDir(string dir)
{
	Door* d = nullptr;
	for (int i = 0; i < doors.size(); i++)
		if (doors[i].GetDirection() == dir)
			d = &doors[i];
	return d;
}

void Location::AddDoor(Door d)
{
	doors.push_back(d);
}

vector<Enemy>* Location::GetEnemies()
{
	return &enemies;
}

Enemy * Location::GetEnemy(string n)
{
	Enemy* e = nullptr;
	for (int i = 0; i < enemies.size(); i++)
		if (enemies[i].GetName() == n)
			e = &enemies[i];
	return e;
}

void Location::AddEnemy(Enemy e)
{
	enemies.push_back(e);
}

void Location::AddItem(Item* i)
{
	inv.PutItem(i);
}

string Location::GetConnections()
{
	string output = "There are connecting rooms to the ";

	for (int i = 0; i < connections.size(); i++)
	{
		if (i == (connections.size() - 1))
			output += connections[i].direction + "\n";
		else
			output += connections[i].direction + ", ";
	}
	return output;
}