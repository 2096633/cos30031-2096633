#include "Player.h"
#include <iostream>

Player::Player() : GameEntity("player", "this is the player", player)
{
	inventory.PutItem(new Item("Map", "A slightly torn map"));
	health = Health();
	damage = Damage(2);
}

Inventory* Player::GetInventory()
{
	return &inventory;
}

void Player::SetLocation(Location* l)
{
	currentLocation = l;
}

Location* Player::GetLocation()
{
	return currentLocation;
}

Health * Player::GetHp()
{
	return &health;
}

Damage* Player::GetDamage()
{
	return &damage;
}

void Player::SendMessage(Message m)
{
	if(m.GetMessage() == "attack")
	{ }
}