#include "AliasCommand.h"
#include <iostream>

AliasCommand::AliasCommand(CommandManager& c)
{
	cm = &c;
}

void AliasCommand::Execute()
{
	if (input.size() > 3)
	{
		cm->GetCommands()->insert(pair<string, Command*>(input[2], cm->GetCommands()->operator[](input[1])));
		cm->GetCommandList()->push_back(input[2]);
	}
	else
		cout << "alias not added" << endl;
}
