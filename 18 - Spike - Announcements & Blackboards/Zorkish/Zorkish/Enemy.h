#pragma once
#include "GameEntity.h"
#include "Health.h"
#include "Damage.h"
#include "Inventory.h"
#include "Message.h"

class Enemy : public GameEntity
{
public:
	Enemy(std::string n, std::string d, int h, int dmg);
	Inventory* GetInventory();
	Health* GetHp();
	Damage* GetDPS();
	bool IsDed();
	void SendMessage(Message m);
private:
	Damage damage;
	Health health;
	Inventory inv;
	bool isDed;
};
