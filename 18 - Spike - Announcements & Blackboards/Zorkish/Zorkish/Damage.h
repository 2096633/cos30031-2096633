#pragma once

class Damage
{
public:
	Damage();
	Damage(int dmg);
	int GetDamage();
private:
	int damage;
};