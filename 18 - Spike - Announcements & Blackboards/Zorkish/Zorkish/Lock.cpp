#include "Lock.h"

Lock::Lock()
{
	locked = false;
}

Lock::Lock(Item i)
{
	locked = true;
	key = i;
}

bool Lock::IsLocked()
{
	return locked;
}

void Lock::Unlock(Item i)
{
	if (key == i)
		locked = !locked;
}
