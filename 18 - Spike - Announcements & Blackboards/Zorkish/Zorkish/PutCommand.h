#pragma once
#include "Command.h"
#include "Player.h"

class PutCommand : public Command
{
public:
	PutCommand(Player*);
	void Execute();
private:
	Player* player;
};
