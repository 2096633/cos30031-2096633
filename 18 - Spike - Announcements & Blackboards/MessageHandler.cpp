#include "MessageHandler.h"

template <class T>
MessageHandler<T>::MessageHandler(){}

template<class T>
void MessageHandler<T>::ReceiveMessage(Message<T> m)
{
	messages.push_back(m);
}

template<class T>
void MessageHandler<T>::DispatchMessages()
{
	GameEntity* target;
	for (int i = 0; i < messages.size(); i++)
	{
		if (target.GetName() == messages[i].GetTarget().GetName())
			target = messages[i].GetTarget();
	}

	if (target != nullptr)
	{
		target.SendMessage();
	}
}
