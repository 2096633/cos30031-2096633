#pragma once
#include "GameEntity.h"
#include "Item.h"
#include <string>


template <class T>
class GameEntity;

template<class T> 
class Message
{
public:
	Message(GameEntity<T> sender, GameEntity<T> target, std::string type, T data);
	GameEntity<T>* GetSender();
	GameEntity<T>* GetTarget();
	std::string GetMessage();
	T GetData();
private:
	GameEntity<T>* sender;
	GameEntity<T>* target;
	std::string message;
	T data;
};