#pragma once
#include "Message.h"
#include "GameEntity.h"
#include <vector>
#include <string>

template <class T>
class MessageHandler
{
public:
	MessageHandler();
	void ReceiveMessage(Message);
	void DispatchMessages();
private:
	std::vector<GameEntity*> entities;
	std::vector<Message> messages;
};
